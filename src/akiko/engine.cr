require "./http"
require "json"
require "colorize"
require "./config"

module Akiko
  <<-DOC
    Every single new engine from the config should have the properties of *token*,
    *binary* and *name* at the very least.
  DOC

  abstract class ChessEngine
    enum Status
      Halted  # => 0
      Running # => 1
    end

    def initialize(env : Config)
      env
    end
  end

  class EngineSpawner
  end

  class Engine < ChessEngine
    CONFIG = Config.new

    # This function should authorize the bot with the provided *token* & automatically upgrade it to a bot account.
    # Smol feature but I think it's still cool.
    def authorize(config : Config)
      request_token = "Bearer #{config.token}"
      AkHTTP.post("/api/account", HTTP.Headers{"Authorization" => request_token})
    end

    def run(@engine : String, @binary : String)
      cmd = "./engines/#{engine}/#{binary}"
      {% if flag?(:linux) %}
        # Linux
        spawn Process.exec("sh", {"-c", cmd})
      {% elsif flag?(:darwin) %}
        # Mac
        spawn Process.exec("sh", {"-c", cmd})
      {% elsif flag?(:win32) %}
        # Windows
        spawn Process.exec({cmd})
      {% end %}
    end

    def restart
      Fiber.yield
      run @engine, @binary
    end

    def halt
      Fiber.yield
    end
  end
end
