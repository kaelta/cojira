require "yaml"

module Akiko
  enum Mode
    Development # => 0
    Production  # => 1
    Testing     # => 2
  end

  class Config
    def initialize
      config = YAML.parse(File.open("./config.yml"))
      config
    end

    def engines
      engines = self.config

      puts engines

      engines
    end
  end
end
