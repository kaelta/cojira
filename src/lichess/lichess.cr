require "../board/constants"
require "http/client"

module Lichess
  def acceptChallenge(challengeId : String, bearer : String)
    client = HTTP::Client.new

    client.post(
      "https://lichess.org/api/challenge/#{challengeId}/accept",
      HTTP::Headers.new({Authorization => "Bearer " + bearer})
    ) do |response|
      unless response.status_code != 200

      end
    end
  end
end
